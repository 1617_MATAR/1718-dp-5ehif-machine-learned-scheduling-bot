import * as request from "request";
import { Options } from "request";
import { apiConfig } from "./config/config";
import { LoginRequest } from "./schemas/LoginRequest";
import { ChangeRequest } from "./schemas/ChangeRequest";
import { User, Shift } from "./schemas/Commons";
import { ConfirmChangeRequest } from "./schemas/ConfirmChangeRequest";
import { Candidate } from "./schemas/Candidate";
import { CandidatesRequest } from "./schemas/CandidatesRequest";
import { DBUser } from "./database";

enum Routes {
  LOGIN = "/login",
  REQUEST_CHANGE = "/state/requestchange",
  CANCEL_CHANGE_REQUEST = "/state/requestchange/cancel",
  CONFIRM_SHIFT = "/state/requestchange/confirm",
  NO_CANDIDATE_ACCEPTED = "/state/candidates/noneaccepted",
  CYCLE_TIMEOUT = "/state/candidates/cycletimeout",
  CANDIDATE_ACCEPTED = "/state/candidates/accepted",
  NEXT_CANDIDATES = "/state/candidates/noneaccepted"
}

class ErrorResponse {
  errorCode: number;
  message: string;
  constructor(errorCode: number, message: string) {
    this.errorCode = errorCode;
    this.message = message;
  }
}

class RestClient {
  private baseEndpoint: string;
  private static jwtToken: string;

  constructor() {
    this.baseEndpoint = `${apiConfig.protocol}://${apiConfig.url}:${
      apiConfig.port
    }/${apiConfig.endpoint}`;
  }

  private login(): Promise<any> {
    let data: LoginRequest = {
      username: apiConfig.credentials.username,
      password: apiConfig.credentials.password
    };
    return this.post(Routes.LOGIN, data).then(response => {
      if (response.headers["authorization"]) {
        RestClient.jwtToken = response.headers["authorization"] as string;
      }
    });
  }

  post(
    route: Routes,
    postBody: object,
    sessionToken?: string
  ): Promise<request.Response> {
    console.log(route + ":" + JSON.stringify(postBody) + ":" + sessionToken);
    return new Promise((resolve, reject) => {
      request.post(
        this.getOptions(route, sessionToken, postBody),
        (error: any, response: request.Response, body: any) => {
          if (error) {
            reject(error);
          } else if (response.statusCode === 401) {
            this.login()
              .then(() => this.post(route, postBody, sessionToken))
              .then(resolve)
              .catch(reject);
          } else if (response.statusCode === 200) {
            resolve(response);
          } else {
            reject(this.handleApiErrors(body));
          }
        }
      );
    });
  }

  get(route: Routes, sessionToken?: string): Promise<request.Response> {
    return new Promise((resolve, reject) => {
      request.get(
        this.getOptions(route, sessionToken),
        (error: any, response: request.Response, body: any) => {
          if (error) {
            reject(error);
          } else if (response.statusCode === 401) {
            this.login()
              .then(() => this.get(route))
              .then(resolve)
              .catch(reject);
          } else if (response.statusCode === 200) {
            resolve(response);
          } else {
            reject(this.handleApiErrors(body));
          }
        }
      );
    });
  }

  private handleApiErrors(body: any): ErrorResponse {
    let parsedBody = JSON.parse(body);
    if (parsedBody && parsedBody["errorCode"] && parsedBody["error"]) {
      let errorCode = parsedBody["errorCode"];
      let message = parsedBody["error"];
      return new ErrorResponse(errorCode, message);
    }
  }

  private getOptions(
    route: string,
    sessionToken?: string,
    body?: object
  ): request.CoreOptions & request.UrlOptions {
    let options: any = {
      headers: this.buildHeaders(sessionToken),
      url: `${this.baseEndpoint}/${route}`
    };

    if (body) {
      options.body = JSON.stringify(body);
    }

    return options;
  }

  private buildHeaders(sessionToken?: string): object {
    let headers: any = {};

    if (RestClient.jwtToken) {
      headers["content-type"] = "text/plain";
      headers.authorization = RestClient.jwtToken;
    } else {
      headers["content-type"] = "application/json";
    }

    if (sessionToken) {
      headers.sessionToken = sessionToken;
    }

    return headers;
  }
}

export class SheepblueRestClient extends RestClient {
  public requestChange(user: User, day: number): Promise<request.Response> {
    let data: ChangeRequest = {
      user: user,
      day: day
    };
    return super.post(Routes.REQUEST_CHANGE, data);
  }
  public confirmShift(
    shift: Shift,
    sessionToken: string
  ): Promise<request.Response> {
    let data: ConfirmChangeRequest = {
      shift: shift
    };
    return super.post(Routes.CONFIRM_SHIFT, data, sessionToken);
  }
  public checkCancel(sessionToken: string): Promise<request.Response> {
    return super.get(Routes.CANCEL_CHANGE_REQUEST, sessionToken);
  }
  public candidateAccepted(requester: DBUser) {
    let request: CandidatesRequest = {
      shiftId: requester.requesterContext.selectedShift.id,
      candidates: requester.requesterContext.candidates,
      cycleInfo: requester.requesterContext.cycleInfo
    };

    return super.post(
      Routes.CANDIDATE_ACCEPTED,
      request,
      requester.requesterContext.sessionToken
    );
  }
  public getNextCandidates(requester: DBUser) {
    let request: CandidatesRequest = {
      shiftId: requester.requesterContext.selectedShift.id,
      candidates: requester.requesterContext.candidates,
      cycleInfo: requester.requesterContext.cycleInfo
    };
    return super.post(
      Routes.NEXT_CANDIDATES,
      request,
      requester.requesterContext.sessionToken
    );
  }
  public timeoutCompletely(requester: DBUser) {
    let request: CandidatesRequest = {
      shiftId: requester.requesterContext.selectedShift.id,
      candidates: requester.requesterContext.candidates,
      cycleInfo: requester.requesterContext.cycleInfo
    };
    return super.post(
      Routes.CYCLE_TIMEOUT,
      request,
      requester.requesterContext.sessionToken
    );
  }
}
