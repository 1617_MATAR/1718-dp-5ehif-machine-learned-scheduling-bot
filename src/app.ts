import { SheepblueBot } from "./bot";
import * as express from "express"
import { appConfig, localizationConfiuration } from "./config/config";
import * as i18next from "i18next"
import * as moment from "moment";
const bot = SheepblueBot.Instance

const app = express()
app.use(appConfig.path + '/messages', bot.Router)

i18next.init({
    lng: appConfig.locale,
    debug: false,
    interpolation: {
        format: (value: any, format?: any, lng?: any) => {
            if (format) {
                if (value instanceof Date) {
                    return moment(value).format(format)
                }
            }
            return value
        }
    },
    resources: localizationConfiuration
}, () => {
    console.log("Initialized localization")
})
export = app