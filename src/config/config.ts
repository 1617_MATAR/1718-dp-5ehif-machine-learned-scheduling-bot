import { Resource } from 'i18next'

export let appConfig = {
  locale: process.env.BOT_LOCALE || 'de',
  path: process.env.BOT_BASE || '/api/bot',
  port: process.env.BOT_PORT || 3978
}

export let apiConfig = {
  protocol: process.env.API_PROTOCOL || 'http',
  url: process.env.API_URL || 'localhost',
  port: process.env.API_PORT || 8081,
  endpoint: process.env.API_BASE || 'api',
  credentials: {
    username: process.env.API_CREDENTIALS_USERNAME || 'BotService',
    password: process.env.API_CREDENTIALS_PASSWORD || 'bot'
  }
}

export let luisConfig = {
  url: {
    ['de' as string]: process.env.LUIS_DE_PATH || 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/4345f465-f39e-4a71-8855-bc1d3ab1223c?subscription-key=04f4d4f13a9249e0b5b8ddce1c82367a&verbose=true&timezoneOffset=0&bing-spell-check-subscription-key=96413547ba3c4436862bc5b348571bad&q=',
    ['en' as string]: process.env.LUIS_EN_PATH || 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/55b29e75-91c3-4887-b873-f5c3be1d41ba?subscription-key=3edce21f3d0f4dfe912bf0c423bd3fb8&verbose=true&timezoneOffset=0&q='
  }
}

export let botConfig = {
  connectorSettings: {
    appId: process.env.BOT_APP_ID || '44007d8c-2839-437d-9d31-dfdbc8cfef66',
    appPassword: process.env.BOT_APP_PASSWORD || 'H$r9cII9O9+*6rf#',
    stateEndpoint: process.env.BotStateEndpoint,
    openIdMetadata: process.env.BotOpenIdMetadata
  }
}

export let localizationConfiuration: Resource = {
  de: {
    translation: {
      general: {
        hello:
          'Hallo! Ich helfe dir deine Arbeitszeiten besser zu organisieren.',
        greeting: 'Hallo! :)',
        ok: 'OK!',
        thanks: 'OK. Danke sehr!',
        thanksForAnswering: 'OK. Danke für deine Antwort!'
      },
      help: {
        ['general' as string]:
          'Ich bin ein Bot, der  dir die Arbeit erleichtert.\n\n' +
          'Wenn du an einem Tag nicht arbeiten kannst, sag mir bescheid und ich finde dir einen Ersatz.\n\n' +
          'Ein Beispiel wäre "Ich kann nächsten Freitag um 7:45 nicht arbeiten."\n\n' +
          'Oder: "Finde mir einen Ersatz für den 24. Mai."'
      },
      feedback: {
        letsCheck: 'OK, ich schau mal was ich machen kann',
        confirmReplacement: 'Jemand ist für dich eingesprungen :)\n\nSchönen restlichen Tag noch!',
        checkCancel: 'Ich check mal, ob ich das rückgängig machen kann...',
        cancelWorked: 'Hat geklappt. Sag mir bescheid, sobald du etwas von mir brauchst!',
        processingChosenShift: 'Ich frag mal rum, melde mich dann wieder! :)',
        ifYouNeedAnything: 'Passt. Wenn du noch was brauchst, sag bescheid.',
        alreadyGotResponse: 'Hat sich erübrigt, danke trotzdem!'
      },
      exceptions: {
        somethingWrong: 'Ups, etwas ist schiefgelaufen!',
        doesntWorkThatDay: 'Es sieht so aus als ob du nicht an diesen Tag arbeitest.',
        noApplicableShiftThatDay: 'Es gibt an dem Tag keine Schicht die in Frage kommt.',
        missunderstood: 'Ich hab dich leider nicht verstanden :(\n\nFalls du hilfe brauchst, schrei einfach HILFE :)',
        nooneFound: 'Ich hab leider niemanden gefunden, der erreichbar ist :(',
        inThePast: '{{datetime, DD. MM. YYYY HH:mm}} liegt in der Vergangenheit!',
        shiftAlreadyStarted: 'Die Schicht hat schon angefangen, ich kann sie nicht mehr ändern!',
        cantCancel: 'Leider geht das nicht mehr',
        askForDateAgain: 'Bitte überprüfe deine Eingabe, ich konnte kein Datum erkennen',
        nobodyAnswered: 'Leider hat niemand angenommen... :('
      },
      questions: {
        askForDate: 'Kannst du mir ein Datum oder eine Uhrzeit geben?',
        confirmTime: 'Hey, kannst du am {{date, DD. MM. YYYY}} um {{time, HH:mm}} arbeiten?'
      },
      userInput: {
        cancelSelection: 'Lieber doch nicht.',
        confirmShift: 'Bitte bestätige mir deine Schicht:'
      },
      unknownNegative: 'Was nicht?',
      fromTimeUntilTime: '{{from, HH:mm}} bis {{until, HH:mm}}'
    }
  }
}
