import { User } from "./Commons";

export interface ChangeRequest {
    user: User,
    day: number
}