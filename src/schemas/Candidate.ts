import { User } from './Commons'

export interface Candidate {
  user: User
  candidateState: States
}

export enum States{
  ASKED = "ASKED",
  NOT_ASKED = "NOT_ASKED",
  REPLIED = "REPLIED",
  DENIED = "DENIED"
}