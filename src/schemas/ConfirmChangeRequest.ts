import { Shift } from "./Commons";

export interface ConfirmChangeRequest {
    shift: Shift
}