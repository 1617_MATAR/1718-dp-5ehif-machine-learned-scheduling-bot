export interface Shift {
  id: number
  eventId: String
  fromDate: number
  toDate: number
  user: User
}

export interface User {
  username: string
  email: string
}
