import { Candidate } from "./Candidate";

export interface CycleInfo {
  maxCycles?: number;
  cycleTime?: number;
  cyclesLeft?: number;
}

export interface CandidatesRequest {
  shiftId: number;
  candidates: Array<Candidate>;
  cycleInfo: CycleInfo;
}
