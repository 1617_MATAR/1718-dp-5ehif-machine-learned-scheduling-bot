import { SheepblueRestClient } from "./restclient";
import { Chrono } from "./chrono";
import {
  LuisRecognizer,
  UniversalBot,
  ChatConnector,
  IntentDialog,
  Session,
  IAddress,
  Message,
  AttachmentLayout,
  CardAction,
  HeroCard,
  MemoryBotStorage,
  EntityRecognizer,
  Prompts,
  Dialog
} from "botbuilder";
import { luisConfig, appConfig, botConfig } from "./config/config";
import { Database, DBUser, CandidateContext } from "./database";
import * as i18next from "i18next";
import { Shift, User } from "./schemas/Commons";
import { Candidate, States } from "./schemas/Candidate";
import * as express from "express";

enum Dialogs {
  ENTRY = "/",
  CONFIRM_SHIFT = "/confirmshift",
  GET_DATE = "/get_date",
  ASK_CANDIDATE = "/ask_candidate"
}

export class SheepblueBot {
  private static _instance: SheepblueBot;
  private restClient: SheepblueRestClient;
  private chrono: Chrono;
  private database: Database;
  private recognizer: LuisRecognizer;
  private connector: ChatConnector;
  private bot: UniversalBot;
  private listener: (req: any, res: any) => void;
  private router: express.Router;

  private constructor() {
    this.restClient = new SheepblueRestClient();
    this.database = Database.Instance;
    this.chrono = new Chrono();
    this.recognizer = new LuisRecognizer(luisConfig.url[appConfig.locale]);
    this.connector = new ChatConnector(botConfig.connectorSettings);
    this.listener = this.connector.listen();
    this.router = express.Router();
    this.router.post("/", (req, res) => {
      this.listener(req, res);
    });
    this.bot = this.createBot();
    this.bot.dialog(Dialogs.ENTRY, this.entryPoint());
    this.bot.dialog(Dialogs.CONFIRM_SHIFT, (req, res, next) =>
      this.chooseShiftDialog(req, res, next)
    );

    this.bot.dialog(Dialogs.GET_DATE, [
      (session, args) => {
        if (args && args.reprompt) {
          Prompts.text(session, i18next.t("exceptions.askForDateAgain"));
        } else {
          Prompts.text(session, i18next.t("questions.askForDate"));
        }
      },
      (session, args) => {
        let time = this.chrono.getStartTime(session.message.text);
        if (!time) {
          session.replaceDialog(Dialogs.GET_DATE, { repromt: true });
        } else {
          args.response = time;
          session.endDialogWithResult(args);
        }
      }
    ]);
  }

  private getNewCandidates(user: DBUser) {
    this.restClient.getNextCandidates(user.candidateContext.requester);
  }

  private allDenied(user: DBUser): boolean {
    let candidates =
      user.candidateContext.requester.requesterContext.candidates;

    let anyNotDenied;
    candidates.forEach(candidate => {
      if (candidate.candidateState !== States.DENIED) {
        anyNotDenied = true;
      }
    });

    return anyNotDenied;
  }

  private anyWritten(user: DBUser): boolean {
    let candidates =
      user.candidateContext.requester.requesterContext.candidates;

    let anyWritten;
    candidates.forEach(candidate => {
      if (candidate.candidateState === States.ASKED) {
        anyWritten = true;
      }
    });

    return anyWritten;
  }

  private setCandidateState(user: DBUser, state: States) {
    let candidates =
      user.candidateContext.requester.requesterContext.candidates;

    candidates.forEach(candidate => {
      if (
        candidate.user.username &&
        candidate.user.username === user.username
      ) {
        candidate.candidateState = state;
      }
    });

    user.candidateContext.requester.requesterContext.candidates = candidates;

    this.database.updateRequesterContext(user.candidateContext.requester);
  }

  private createBot(): UniversalBot {
    return new UniversalBot(this.connector).set(
      "storage",
      new MemoryBotStorage()
    );
  }

  private entryPoint(): IntentDialog {
    return new IntentDialog({
      recognizers: [this.recognizer]
    })
      .matches("Greeting", session => {
        this.database.updateAddress(session.message.address);
        session.send(i18next.t("general.greeting"));
      })
      .matches("Help", session => {
        this.database.updateAddress(session.message.address);
        session.send(i18next.t("help.general"));
      })
      .matches("Change Request", [
        (session, args) => {
          this.database.updateAddress(session.message.address);
          let time = this.chrono.getStartTime(session.message.text);
          if (time) {
            this.executeChangeRequest(time, session);
          } else {
            session.beginDialog(Dialogs.GET_DATE);
          }
        },
        (session, args) => {
          if (args.childId === `*:${Dialogs.GET_DATE}`) {
            this.executeChangeRequest(args.response, session);
          }
        }
      ])
      .matches("Positive Confirmation", (session, args) => {
        let user = this.database.updateAddress(session.message.address);
        let requester = user.candidateContext.requester;
        if (user && requester && !requester.requesterContext.gotResponse) {
          requester.requesterContext.gotResponse = true;
          this.database.updateRequesterContext(requester);

          session.send(i18next.t("general.thanks"));
          this.setCandidateState(user, States.REPLIED);

          this.restClient
            .candidateAccepted(requester)
            .then(_ => {
              this.sendMessage(
                requester.address,
                i18next.t("feedback.confirmReplacement")
              );
              this.cleanupConversations(requester);
              session.endDialog();
            })
            .catch(_ => {
              this.sendMessage(
                requester.address,
                i18next.t("exceptions.somethingWrong")
              );
              this.cleanupConversations(requester);
              session.endDialog();
            });
        } else {
          session.send(i18next.t("general.ok"));
        }
      })
      .matches("Negative Confirmation", (session, args) => {
        let user = this.database.updateAddress(session.message.address);
        let requester = user.candidateContext.requester;
        if (requester && !requester.requesterContext.gotResponse) {
          session.send(i18next.t("general.thanksForAnswering"));
          this.setCandidateState(user, States.DENIED);
          if (this.allDenied(user)) {
            this.getNewCandidates(user);
          }
        } else {
          session.send(i18next.t("unknownNegative"));
        }
      })
      .matches("Cancel Request", session => {
        let user = this.database.updateAddress(session.message.address);
        session.send(i18next.t("feedback.checkCancel"));
        this.restClient
          .checkCancel(user.requesterContext.sessionToken)
          .then(__ => session.send(i18next.t("feedback.cancelWorked")))
          .catch(err => {
            session.send(i18next.t("exceptions.cantCancel"));
          });
      })
      .onDefault(session => {
        let user = this.database.updateAddress(session.message.address);
        session.send(i18next.t("exceptions.missunderstood"));
      });
  }

  private cleanupConversations(requester: DBUser): void {
    if (!requester || !requester.requesterContext.candidates) {
      return;
    }

    requester.requesterContext.candidates.forEach(candidate => {
      if (candidate && candidate.user && candidate.user.username) {
        let dbuser = this.database.getUserByName(candidate.user.username);
        if (dbuser) {
          dbuser.candidateContext.requester = undefined;
          this.database.updateCandidateContext(dbuser);
          if (dbuser && candidate.candidateState === States.ASKED) {
            this.sendMessage(
              dbuser.address,
              i18next.t("feedback.alreadyGotResponse")
            );
          }
        }
      }
    });

    requester.requesterContext = {
      cycleInfo: {}
    };
    this.database.updateRequesterContext(requester);
  }

  private executeChangeRequest(time: Date, session: Session) {
    session.send(i18next.t("feedback.letsCheck"));
    session.sendBatch();
    console.log("Time:" + time);
    console.log("Milliseconds: " + time.getTime());
    let user = this.database.updateAddress(session.message.address);
    this.restClient
      .requestChange(
        { username: user.username, email: "email" },
        time.getTime()
      )
      .then(response => {
        let sessionToken = response.headers["sessiontoken"] as string;
        user.requesterContext.sessionToken = sessionToken;
        this.database.updateRequesterContext(user);

        let body = JSON.parse(response.body);
        let shifts = body.data.shifts;

        if (shifts && shifts.length) {
          if (this.anyInFuture(shifts)) {
            user.requesterContext.shifts = shifts;
            this.database.updateRequesterContext(user);
            session.beginDialog(Dialogs.CONFIRM_SHIFT);
          } else {
            session.send(i18next.t("exceptions.noApplicableShiftThatDay"));
          }
        } else {
          session.send(i18next.t("exceptions.doesntWorkThatDay"));
        }
      })
      .catch(err => {
        console.error(err);
        if (err && err.errorCode == 108) {
          session.send(i18next.t("exceptions.inThePast", { datetime: time }));
        } else {
          session.send(i18next.t("exceptions.somethingWrong"));
        }
      });
  }

  private anyInFuture(shifts: Array<Shift>) {
    for (let i = 0; i < shifts.length; i++) {
      const element = shifts[i];
      if (element.fromDate > new Date().getTime()) {
        return true;
      }
    }
    return false;
  }

  private timeoutFully(address: IAddress) {
    let user: DBUser = this.database.updateAddress(address);
    user = this.database.updateAddress(user.address);
    if (
      user.requesterContext.sessionToken &&
      !user.requesterContext.gotResponse
    ) {
      console.error("Nobody answered to: " + user.username);
      this.restClient
        .timeoutCompletely(user)
        .then(val => {
          this.sendMessage(
            user.address,
            i18next.t("exceptions.nobodyAnswered")
          );
          this.cleanupConversations(user);
        })
        .catch(err => {
          console.error(err);
          this.sendMessage(
            user.address,
            i18next.t("exceptions.somethingWrong")
          );
          this.cleanupConversations(user);
        });
    }
  }

  private cycleTimeout(address: IAddress) {
    let user: DBUser = this.database.updateAddress(address);
    user.requesterContext.cycleInfo.cyclesLeft--;
    user = this.database.updateRequesterContext(user);
    console.log("Cycle Timeout for: " + user.username);
    if (
      user.requesterContext.sessionToken &&
      !user.requesterContext.gotResponse
    ) {
      this.restClient
        .getNextCandidates(user)
        .then(response => {
          let body = JSON.parse(response.body);
          if (body.data.candidates) {
            let candidates: Array<Candidate> = body.data.candidates;
            let countOfWrittenCandidates = this.writeCandidates(
              candidates,
              user.requesterContext.selectedShift,
              user
            );
            user.requesterContext.candidates = candidates;
            this.database.updateRequesterContext(user);
            if (
              countOfWrittenCandidates == 0 &&
              user.requesterContext.cycleInfo.cyclesLeft > 0
            ) {
              this.cycleTimeout(address);
            } else if (user.requesterContext.cycleInfo.cyclesLeft > 0) {
              setTimeout(
                () => this.cycleTimeout(address),
                user.requesterContext.cycleInfo.cycleTime
              );
            } else if (this.anyWritten(user)) {
              setTimeout(
                () => this.timeoutFully(user.address),
                user.requesterContext.cycleInfo.cycleTime
              );
            } else {
              this.timeoutFully(user.address);
            }
          }
        })
        .catch(response => {
          console.error(response);
          this.sendMessage(
            user.address,
            i18next.t("exceptions.somethingWrong")
          );
          this.cleanupConversations(user);
        });
    }
  }

  private writeCandidates(
    candidates: Array<Candidate>,
    shift: Shift,
    requester: DBUser
  ): number {
    let countOfWrittenCandidates: number = 0;
    for (const candidate of candidates) {
      let candidateUser: DBUser = this.database.getUserByName(
        candidate.user.username
      );
      if (
        candidateUser &&
        candidate.candidateState === States.NOT_ASKED &&
        !requester.requesterContext.gotResponse
      ) {
        candidates[candidates.indexOf(candidate)].candidateState = States.ASKED;
        countOfWrittenCandidates++;
        let time = new Date(shift.fromDate);
        candidateUser.candidateContext.requester = requester;
        this.database.updateCandidateContext(candidateUser);
        this.sendMessage(
          candidateUser.address,
          i18next.t("questions.confirmTime", { date: time, time: time })
        );
      }
    }
    return countOfWrittenCandidates;
  }

  private chooseShiftDialog(session: Session, args: any, next: any) {
    let user = this.database.updateAddress(session.message.address);
    let msg = new Message(session);
    msg.attachmentLayout(AttachmentLayout.list);

    if (session && user.requesterContext.shifts) {
      let buttons: Array<CardAction> = this.getShiftButtons(
        session,
        user.requesterContext.shifts
      );
      msg.addAttachment(
        new HeroCard(session)
          .title(i18next.t("userInput.confirmShift"))
          .buttons(buttons)
      );
    }

    let postBackMessage: string = session.message.text;
    let index: number = Number.parseInt(
      postBackMessage.substr(1, postBackMessage.length)
    );

    if (!Number.isNaN(index)) {
      session.send(i18next.t("feedback.processingChosenShift"));

      user.requesterContext.selectedShift = user.requesterContext.shifts[index];
      user = this.database.updateRequesterContext(user);

      this.restClient
        .confirmShift(
          user.requesterContext.selectedShift,
          user.requesterContext.sessionToken
        )
        .then(response => {
          let body = JSON.parse(response.body);
          if (body.data.candidates) {
            let candidates: Array<Candidate> = body.data.candidates;
            let countOfWrittenCandidates = this.writeCandidates(
              candidates,
              user.requesterContext.selectedShift,
              user
            );
            if (!countOfWrittenCandidates) {
              user.requesterContext.cycleInfo = body.data.cycleInfo;
              user.requesterContext.candidates = candidates;
              user = this.database.updateRequesterContext(user);
              this.cycleTimeout(user.address);
            } else {
              user.requesterContext.cycleInfo = body.data.cycleInfo;
              setTimeout(
                () => this.cycleTimeout(user.address),
                user.requesterContext.cycleInfo.cycleTime
              );
              user.requesterContext.candidates = candidates;
              user = this.database.updateRequesterContext(user);
            }
            session.endDialog();
          }
        })
        .catch(err => {
          console.error(err);
          if (err && err.errorCode == 108) {
            session.send(i18next.t("exceptions.shiftAlreadyStarted"));
          }
          session.send(i18next.t("exceptions.somethingWrong"));
          session.endDialog();
        });
    } else if (postBackMessage === "Cancel") {
      session.send(i18next.t("feedback.ifYouNeedAnything"));
      session.endDialog();
    } else {
      session.send(msg);
    }
  }

  private getShiftButtons(
    session: Session,
    shifts: Array<Shift>
  ): Array<CardAction> {
    let buttons: Array<CardAction> = [];

    for (const shift of shifts) {
      let fromDate = new Date(shift.fromDate);
      let toDate = new Date(shift.toDate);
      let text = i18next.t("fromTimeUntilTime", {
        from: fromDate,
        until: toDate
      });
      buttons.push(
        CardAction.postBack(session, "_" + shifts.indexOf(shift), text)
      );
    }
    buttons.push(
      CardAction.postBack(
        session,
        "Cancel",
        i18next.t("userInput.cancelSelection")
      )
    );
    return buttons;
  }

  private sendMessage(address: IAddress, message: string) {
    this.bot.send(new Message().address(address).text(message));
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public get Router() {
    return this.router;
  }
}
