import * as LokiConstructor from "lokijs";
import { ChangeRequest } from "./schemas/ChangeRequest";
import { IAddress } from "botbuilder";
import { Shift } from "./schemas/Commons";
import { request } from "http";
import { Candidate } from "./schemas/Candidate";
import { CycleInfo } from "./schemas/CandidatesRequest";

export class Database {
  private static _instance: Database;
  private db: Loki;
  private users: Collection<DBUser>;

  private constructor() {
    this.db = new LokiConstructor("loki.db", {
      autosave: true,
      autosaveInterval: 500,
      autoload: true,
      autoloadCallback: () => this.autoLoad()
    });
  }

  private autoLoad() {
    console.log("AutoLoaded");
    let coll = this.db.getCollection("users");
    if (!coll) {
      coll = this.db.addCollection("users");
    }
    this.users = coll;
  }

  private addUser(address: IAddress): DBUser {
    return this.users.insert({
      username: address.user.name,
      address: address,
      requesterContext: {
        cycleInfo: {}
      },
      candidateContext: {}
    });
  }

  public updateAddress(address: IAddress): DBUser {
    let user: DBUser = this.users.findOne({ username: address.user.name });

    if (!user && address.user.name) {
      user = this.addUser(address);
    }

    if (user) {
      user.address = address;
      return this.users.update(user);
    }
    return undefined;
  }
  public updateCandidateContext(userParam: DBUser): DBUser {
    let user: DBUser = this.updateAddress(userParam.address);
    user.candidateContext = userParam.candidateContext;
    return this.users.update(user);
  }
  public updateRequesterContext(userParam: DBUser): DBUser {
    let user: DBUser = this.updateAddress(userParam.address);
    user.requesterContext = userParam.requesterContext;
    return this.users.update(user);
  }

  public getUserByName(username: string): DBUser {
    return this.users.findOne({ username: username });
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }
}

export interface CandidateContext {
  requester?: DBUser;
}

export interface RequesterContext {
  shifts?: Array<Shift>;
  selectedShift?: Shift;
  candidates?: Array<Candidate>;
  gotResponse?: boolean;
  cycleInfo: CycleInfo;
  sessionToken?: string;
}

export interface DBUser {
  username: string;
  address: IAddress;
  candidateContext: CandidateContext;
  requesterContext: RequesterContext;
}
