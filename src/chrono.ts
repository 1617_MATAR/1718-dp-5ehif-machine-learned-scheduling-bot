//Require chrono module the old way
let chrono = require('chrono-node')

export class Chrono {
    public getTimesArray(message: string) {
        var parsedTime = chrono.de.parse(message)
        if (parsedTime) {
            return parsedTime
        } else {
            return null
        }
    }
    public getChrono() {
        return chrono
    }
    public getStartTime(message: string): Date {
        var parsedTime = chrono.de.parse(message)
        if (parsedTime[0]) {
            console.log("Understood time: " + parsedTime[0].start.date())
            return parsedTime[0].start.date()

        } else {
            return null
        }
    }
    public getEndTime(message: string): Date {
        var parsedTime = chrono.de.parse(message)
        if (parsedTime[1]) {
            return parsedTime[1].end.date()
        } else {
            return null
        }
    }
    public getTimeAsText(message: string): string {
        var parsedTime = chrono.de.parse(message)
        if (parsedTime[0]) {
            return parsedTime[0].text
        } else {
            return null
        }
    }
}