# Configuring The Services

* it is _*IMPORTANT*_!! that you change any security related things.
* the backend has its own config and repo
* the management ui has its own config and repo

## First things first

* Download the project
* npm install
* npm install -g typescript
* run "tsc" whenever you change anything
* when all the config steps are done
  * npm start or click debug in vscode

## Configuration

You can either configure the config.ts file or set environment variables

In azure you will need cognitive services, on luis.ai your bots, a ms app and a web app bot.

### config.ts

* under localizationConfiguration you can change the translations that are available or add new ones.
* under botConfig you can change the app id and password that you need to run the service
* under luisConfig you can change the luisEndpoint for each locale, and its features(using url parameters).
* under apiConfig you can change the credentials and the backend endpoint, port etc.
* under appConfig you can change the locale, base endpoint and port of the service

### Environment Variables

* appConfig
  * BOT_LOCALE
    * bot locale
  * BOT_BASE
    * base endpoint
  * BOT_PORT
    * service port
* apiConfig
  * API_PROTOCOL
    * the protocol to use when connecting to the backend (http/https)
  * API_URL
    * the hostname and domain prefix of the backend
  * API_PORT
  * API_BASE
    * base endpoint of the api
  * credentials
    * API_CREDENTIALS_USERNAME
      * username to login with
    * API_CREDENTIALS_PASSWORD
      * password to login with
* luisConfig
  * LUIS_DE_PATH
    * the url to the de locale bot
  * LUIS_EN_PATH
    * the url to the en locale bot
* botConfig
  * BOT_APP_ID
    * MS app id for the service
  * BOT_APP_PASSWORD
    * ... and the password
